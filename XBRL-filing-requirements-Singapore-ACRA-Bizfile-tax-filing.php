<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>XBRL Filing Services in Singapore</h3>
					</div>
				</div>
				<div class="col-lg-8">
					

					<h4>Who is required to submit financial statements in XBRL format?</h4>
					<p>As per the revised ACRA XBRL filing requirements, from 3 March 2014, the following class of companies are required to submit their financial statements in XBRL Format to ACRA:</p>
					<div class="row top-buffer">
						<div class="col-md-1 ">
							
						</div>
						<div class="col-md-10">
							
							 <ul>
								<li>Singapore incorporated companies which are unlimited</li>
								<li>Singapore incorporated companies which are limited by shares</li>
							   </ul>

						</div>
						<div class="col-md-1">
							
						</div>

					</div>

					<h4 class="top-buffer">What is the applicable financial period for filing in XBRL format?</h4>
				
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li> The revised XBRL filing requirements are applicable to companies, which are filing financial statements with periods ending on or after 30 April 2007</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>


					<h4 class="top-buffer">What Volition can do for you?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>Prepare financial statements in XBRL format.</li>
								<li>Send the XBRL format files to you for filing with ACRA</li>
								<li>Our services also include preparation of Accounts and Bookkeeping</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>


					<h4 class="top-buffer">Whom do we serve?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>We provide XBRL filing from Small Companies to Large Companies.</li>
								<li>Tax Advisors, Accountants and Accounting Firms</li>
								<li>We are also serving Companies, Tax Advisors, Accountants and Accounting Firms.</li>

							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>



					<h4 class="top-buffer">Why Volition?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>Just email us and we will be glad to respond to you. No signing up!</li>
								<li>Provide data in any format like Word or Excel. No uploading!</li>
								<li>We have a team of qualified accountants capable of handling preparing your XBRL Financial Statements with BizFinx filing system</li>
								<li>Review your XBRL tagged accounts and request any number of changes.</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>

					<h4 class="top-buffer">Our fee and pricing parameters?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>Our prices vary for a Company and Tax Advisors, Accountants and Accounting Firms.</li>
								<li>For a Company price depends on Number of Pages.<a href="/contact-us" title="Contact Us"><font color="#0066FF"><strong>Request pricing.</strong></font></a>.</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>

					<div class="row top-buffer">
						<div class="col-md-12">
							
						
					<p>For Outsourcing Accounts, Financial Statement Preparation, Bookkeeping, Payroll and other such services <a href="contact-us" title="Contact Us"><font color="#0066FF"><strong>Write to us!</strong></font></a></p>	
				     </div>
				   </div>



				     <div class="row top-buffer">
						<div class="col-md-4">
							<a href="http://www.xbrlconversionservices.com/sec-xbrl-filing.php" title="United States" style="color:#953735; text-decoration: none; font-weight:bold">XBRL Reporting - US SEC Filings</a>
						</div>

						<div class="col-md-4">
							<a href="http://www.xbrlconversion.net/ixbrl-outsourced-tagging-services-xbrl-outsourcing-conversion-services-hmrc/" title="United Kingdom" style="color:#953735; text-decoration: none; font-weight:bold">iXBRL Services - UK HMRC Reporting</a>
						</div>

						<div class="col-md-4">
							<a href="http://www.xbrlinindia.com/XBRL-filing-requirements-Singapore-ACRA-Bizfile-tax-filing.php" title="Singapore" style="color:#953735; text-decoration: none; font-weight:bold">XBRL Filing - Singapore ACRA Filing</a>
						</div>

					</div>






					<div class="row top-buffer">
						<div class="col-md-6">
								<span class="color"><a href="http://www.xbrlconversionservices.com" style="text-decoration:none; color:#953735; font-weight:bold">Visit Our US Site</a></span>

						</div>
					
				</div>

					
				</div>
				<div class="col-lg-4">
						<!-- Subscription section -->
	<?php include('countrysection.php'); ?>
	<!-- Subscription section end -->

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
