<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>XBRL Outsourcing Services US</h3>
					</div>
				</div>
				<div class="col-lg-8">
					

					<h4>Why Outsource?</h4>
					<div class="row top-buffer">
						<div class="col-md-1 ">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>Your company does not need to buy software</li>
								<li>You don't need to recruit for XBRL</li>
								<li>You save time on training resources</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>

					<h4 class="top-buffer">What we can do for you?</h4>
					<p>We will provide managed block tagging and XBRL detailed tagging services for generating XBRL documents and also conversion services for XBRL reporting for the following:</p>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li> Corporate filings (such as 10Q or 10K) as per the US-SEC requirements</li>
								<li>Mutual fund filings (risk return summary portion)</li>
								<li>US GAAP or IFRS</li>
								<li>Voluntary Filing Program in the iXBRL format</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>


					<h4 class="top-buffer">Why us?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li> A strong pool of XBRL analysts</li>
								<li>Requiring least turn around time</li>
								<li>Our team of experts can save your time & money</li>
								<li>Expertise & understanding of our client's specific needs</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>


					<h4 class="top-buffer">Who we cater to?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>Companies</li>
								<li>Financial Printers</li>
								<li>Investment research firms</li>
								<li>Equity analytics firms</li>
								<li>Audit and Consulting firms looking for shared service models to operate</li>

							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>

					<div class="row top-buffer">
						<div class="col-md-6">
								<span class="color"><a href="http://www.xbrlconversionservices.com" style="text-decoration:none; color:#953735; font-weight:bold">Visit Our US Site</a></span>

						</div>
					
				</div>

					
				</div>
				<div class="col-lg-4">
						<!-- Subscription section -->
	<?php include('countrysection.php'); ?>
	<!-- Subscription section end -->

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
