<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>Privacy Policy</h3>
					</div>
				</div>
				<div class="col-lg-8">
					


					<div class="row top-buffer">
					<div class="col-md-10">
					 <p class="text-justify">Generally, our intent is to collect only the personal information that is provided voluntarily by visitors to our web sites so that we can offer information and/or services to those individuals or offer information about employment opportunities. Please review this Online Privacy Statement ("Online Privacy Statement" or "Privacy Statement") to learn more about how we collect, use, share and protect your personal information that we have obtained.


					 </p>
					
					 <p class="text-justify">No registration is required for you to use Our Site. If you are merely a visitor, we do not collect any personal information about you, except to the limited extent through the use of cookies, which are described below. However, there may be circumstances in which you choose to register or express interest in any of our services or trainings.</p>
				
					  <p class="text-justify">In these cases, Volition LLP may contact registrants to invite them to special events, provide them with information about our services, publications and products, or for other marketing purposes.</p>
					  
					  <p class="text-justify">
					Volition LLP gives you choices regarding the collection and usage of your personally identifiable information. If you have registered for any Volition LLP related updates through this site, and do not wish to receive future e-mails, please visit our unsubscribe page within the application you registered.</p>
					</div>
					</div>

					
				</div>
				<div class="col-lg-4">
						

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
