<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>
	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>Annual Financial Report</h3>
					</div>
				</div>
				<div class="col-lg-8">
					<h4>XBRL Filing Applicability Criteria</h4>
					<p>Vide MCA General Circular No: 16/2012 dated 06.07.2012, a select class of companies<br/><br/>
					● all companies listed with any Stock Exchange(s) in India and their Indian subsidiaries; or<br/>
					● all companies having paid up capital of Rupees five crore and above; or,<br/>
					● all companies having turnover of Rupees one hundred crore and above; or<br/>
					● all companies who were required to file their financial statements for FY 2010-11, using XBRL.<br/><br/>
					and whose accounting year commences on or after 01.04.2011 are mandated to file their financial statements in XBRL using the taxonomy based upon new Schedule VI of the Companies Act, 1956. However, Banking, Power, NBFC and Insurance Companies are exempted from XBRL filing till further order. 
					<br/><br/>
					Companies (with accounting year commencing before 01.04.2011) mandated as per MCA Notification dated 05.10.2011 shall file their financial statements in XBRL using the earlier C&amp;I Taxonomy (based upon old Schedule VI).</p>
					<a href="#" class="site-btn">Cost Audit Report &amp; Compliance Report</a>
				</div>
				<div class="col-lg-4">
					<p>Downlaod MCA Circular on XBRL Applicability</p>
					<a href="https://www.mca.gov.in/Ministry/pdf/General_Circular_16_1_2012_XBRL.pdf" class="site-btn">Circular No: 16/2012</a>
					<p>Apply for XBRL Training</p>
					<a href="#" class="site-btn">Apply for Training</a>

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	<!-- How section -->
	<section class="how-section spad set-bg" data-setbg="img/how-to-bg.jpg">
		<div class="container text-white">
			<div class="section-title">
				<h2>Latest Updates & Circulars</h2>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="how-item">
						<div class="hi-icon">
						<img src="img/icons/p-4.png" alt="">
						</div>
						<h4>MCA C&I Taxonomy</h4>
						<a href="#" class="site-btn">C&I Taxonomy 2016</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="how-item">
						<div class="hi-icon">
							<img src="img/icons/p-4.png" alt="">
						</div>
						<h4>XBRL Business Rules</h4>
						<a href="#" class="site-btn">XBRL Business Rules 2019</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="how-item">
						<div class="hi-icon">
							<img src="img/icons/p-4.png" alt="">
						</div>
						<h4>Download Music</h4>
						<p>Ablandit nunc. Pellentesque id eros venenatis, sollicitudin neque sodales, vehicula nibh. Nam massa odio, porttitor vitae efficitur non, ultric-ies volutpat tellus. </p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- How section end -->

	<!-- Subscription section -->
<?php include('countrysection.php'); ?>
	<!-- Subscription section end -->

<?php
include_once('footer/footer.php');
?>

