<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>iXBRL Mapping & Tagging Services for Ireland</h3>
					</div>
				</div>
				<div class="col-lg-8">
					

					<h4>Who is required to submit financial statements in iXBRL format?</h4>
				
					<div class="row top-buffer">
						<div class="col-md-1 ">
							
						</div>
						<div class="col-md-10">
							
							 <ul>
								<li>Mandatory for Customers of Revenue's Large Cases Division (LCD)</li>
								<li> Mandatory for All Revenue customers filing Corporation Tax returns (Non-large Cases Division)</li>
							   </ul>

						</div>
						<div class="col-md-1">
							
						</div>

					</div>

					<h4 class="top-buffer">When do I start filing in iXBRL format?</h4>
				
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>From 1 May 2014 filing in iXBRL will become mandatory for customers of Revenue's Large Cases Division (LCD)</li>
								<li>From 1 Oct 2014 iXBRL filing is mandatory for all Revenue customers filing Corporation Tax returns (Non-large Cases Division)</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>


					<h4 class="top-buffer">What Volition can do for you?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>Prepare financial statements in XBRL format.</li>
								<li>Send the iXBRL format files to you for filing with the revenue.</li>
								<li>Our services also include Corporation Tax return (CT1) or Income Tax return (Form 11) for filing with the revenue.</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>


					<h4 class="top-buffer">Whom do we serve?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>We provide iXBRL tagging services from Small Companies to Large Companies.</li>
								<li>Tax Advisors, Accountants and Accounting Firms.</li>
								<li>We are also serving Companies, Tax Advisors, Accountants and Accounting Firms of Jersey, Guernsey and Isle of Man.</li>

							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>



					<h4 class="top-buffer">Why Volition?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>Just email us and we will be glad to respond to you. No signing up!</li>
								<li>Provide data in any format like Word or Excel. No uploading!</li>
								<li>We have a team of qualified accountants capable of handling Irish Extension GAAP (IE GAAP) and IFRS.</li>
								<li>Review your iXBRL tagged accounts and request any number of changes.</li>
							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>

					<h4 class="top-buffer">Our fee and pricing parameters?</h4>
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-4">
							
							 <ul>
								<li>Our prices vary for a Company and Tax Advisors, Accountants and Accounting Firms.</li>
								<li>For a Company price depends on Number of Pages.<a href="/contact-us" title="Contact Us"><font color="#0066FF"><strong>Request pricing.</strong></font></a>.</li>
								<li>We could prepare accounts and convert them into iXBRL files at extra cost for filing with the Revenue.</li>
								<li>Special charges for Sole Trader who require to submit financial statements in iXBRL format via ROS.</li>

							   </ul>

						</div>
						<div class="col-md-4">
							
						</div>

					</div>

					<div class="row top-buffer">
						<div class="col-md-6">
								<span ><a href="http://www.xbrlconversionservices.com" style="text-decoration:none; color: #green; font-weight:bold">Visit our iXBRL Ireland site</a></span>

						</div>
					
				</div>

					
				</div>
				<div class="col-lg-4">
						<!-- Subscription section -->
	<?php include('countrysection.php'); ?>
	<!-- Subscription section end -->

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
