<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>Cost Audit Compliance Audit Report</h3>
					</div>
				</div>
				<div class="col-lg-8">
					<h4>XBRL Filing Applicability Criteria</h4>
					<p>It has been decided by the Ministry of Corporate Affairs to mandate the cost auditors and the companies to file Cost Audit Reports (Form-I) and Compliance Reports (Form-A) for the year 2011-12 onwards (including the overdue reports relating to any previous year) in XBRL mode.

					Therefore, filing of existing Form I- Cost Audit Report and Form A- Compliance Report shall not be allowed till 30.06.2012 by which time the new XBRL mode of filing will be ready and enabled.<br><br>

				    <ul class="sub-list-custom">
						<li><a href="#">Download the ICWAI Notification</a></li>
						<li><a href="#">Cost Audit Report & Compliance Filing Date Notification</a></li>
						<li><a href="#">Final Cost Audit Taxonomy and Business Rules 2012</a></li>
						<li><a href="#">Click here for the Final Costing Taxonomy</a></li>
						<li><a href="#">Click here Business Rules Final Costing Taxonomy 2012</a></li>
						<li><a href="#">Validation Tool for Costing Taxonomy Version 1.0 (Beta)</a></li>
				    </ul>

					</p>
					<a href="#" class="site-btn">Click to know about XBRL Services</a>
				</div>
				<div class="col-lg-4">
						<!-- Subscription section -->
	<?php include('countrysection.php'); ?>
	<!-- Subscription section end -->

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->
<?php
include_once('footer/footer.php');
?>

