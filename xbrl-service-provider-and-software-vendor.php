<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h3>About us</h3>
					</div>
				</div>
				<div class="col-md-8">
					<h4></h4>
					<p>
						<a href="/">Volition LLP</a> is a management consulting firm, advising organizations on emerging issues like IFRS and XBRL. <a href="/">Volition LLP</a> has extended its service vertical and now through its subsidiary company Bicon Consultants is capable of providing Asset Valuation Services Property, Plant, Equipment Valuations, Energy Advisory Services, TEV Services, Lender's Independent Engineer's Report and more speak to us or visit <a href="https://biconconsultants.com/">BiconConsultants</a>.
					 Volition LLP has been assisting some of the largest companies of India with IFRS Conversion and is set to bridge the digital divide with XBRL Services and XBRL Software.</p>

					 <div class="row">
					 	<div class="col-md-12">

					 		<img src="img/logo.png" class="img-fluid" alt="Responsive image">


					 	</div>
					 </div>



					 <div class="row top-buffer">
					 	<div class="col-md-12">

					    <p>
						<a href="http://www.reportingstandard.com/en/index.html">Reporting Standard S.L.</a> is an IT company that expands its business activity in three areas: Software development, Consulting Services and development of the XBRL standard. Reporting Standard is a global XBRL Software Vendor and has provided XBRL converter to financial institutions in Europe.</p>


					 	</div>
					 </div>


					 <div class="row">
					 	<div class="col-md-12">

					 		<img src="img/logo.png" class="img-fluid" alt="Responsive image">


					 	</div>
					 </div>

					  <div class="row top-buffer">
					 	<div class="col-md-12">

					    <p>
						<a href="/">Volition LLP</a> along with Reporting Standard will empower organizations to deliver, error free and satisfactory eXBRL reports to the regulators, making us XBRL Software vendor and solution providers of your choice.</p>


					 	</div>
					 </div>

					 <div class="row top-buffer">
					 	<div class="col-md-12">

					    <p>
						The website has been created to provide information on eXBRL, which is required by the Companies, to file their reports as per the XBRL requirements. It is created to bring a clarity on the subject of XBRL in India. To give you an insight of the Implementation Options we provide. Our Methodology on XBRL Reporting, how we can help Companies. Check latest Circulars on XBRL 2017 by MCA; know more.. XBRL Resources will help you know more about XBRL A to Z.
						<a href="xbrl-in-india-circulars.php" style="text-decoration:none; color:#953735">know more..</a></p>

					 	</div>
					 </div>


					 <div class="row top-buffer">
					 	<div class="col-md-12">

					     <ul style="margin-left: 1.5em;">
						<li><a href="http://www.slideshare.net/trivesa/xbrl-conversion-steps" style="text-decoration:none; color:#953735">XBRL Conversion Steps Briefs Download </a></li>
						<li><a href="#" style="text-decoration:none; color:#953735">XBRL Software Download Request</a></li>
						<li><a href="#" style="text-decoration:none; color:#953735">XBRL Validation Tool – Latest Version </a></li>
						<li><a href="#" style="text-decoration:none; color:#953735">We have recently launched XBRL Partner Program – Get Details </a></li>
						
					   </ul>

					 	</div>
					 </div>



					<div class="row top-buffer">
					<div class="col-md-10">
					 <a href="#" class="site-btn">Click to know about XBRL Services</a>
					</div>
					</div>

					
				</div>
   
   <div class="col-lg-4">
						<!-- Subscription section -->
	<?php include('countrysection.php'); ?>
	<!-- Subscription section end -->

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
