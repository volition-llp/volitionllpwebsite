<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>XBRL Outsourcing Services UK</h3>
					</div>
				</div>
				<div class="col-lg-12">
					

					<h4>Why Outsource?</h4>
					<p>The XBRL format being used in the UK for company reporting is known as Inline XBRL or iXBRL. After successfully providing XBRL Conversion services to corporate India, Volition LLP is looking at providing iXBRL Conversion Services to Companies in India.</p>
					<div class="row top-buffer">
						<div class="col-md-1 ">
							
						</div>
						<div class="col-md-11">
							
							 <ul>
								<li>Your company does not need to buy software, continually hire & train new personnel</li>
								<li>We will provide managed tagging services for generating iXBRL documents as per the HMRC mandate in UK and also conversion services for XBRL reporting</li>
								<li>A strong pool of XBRL & iXBRL analysts to requiring least turn around time</li>
								<li>Our team of experts can save your time & money</li>
								<li>Expertise & understanding of our client's specific needs</li>
								<li>We will work for you and with you, cutting out hundreds of man hours associated with XBRL & iXBRL conversion</li>
							   </ul>

						</div>
					
					</div>

					<h4 class="top-buffer">Services</h4>
				
					<div class="row top-buffer">
						<div class="col-md-1">
							
						</div>
						<div class="col-md-11">
							
							 <ul>
								<li>Conversion of your internally produced statutory accounts into iXBRL format, for submission to HMRC</li>
								<li> iXBRL services both to Company and Accounting Firms.</li>
							   </ul>

						</div>
					

					</div>


					<div class="row top-buffer">
						<div class="col-md-6">
								<a href="http://www.xbrlconversion.net" style="text-decoration:none; color:#CC0000; font-weight:bold">Visit Our UK Site</a>

						</div>
					
				</div>

					
				</div>
		
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
