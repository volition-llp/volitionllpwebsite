
	<!-- Footer section -->
	<footer class="footer-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-7 order-lg-2">
					<div class="row">
						<div class="col-sm-4">
							<div class="footer-widget">
								<h2>Menu</h2>
								<ul>
									<li><a href="/<?php echo $var;?>/">Home</a></li>
									<li><a href="#">Circulars</a></li>
									<li><a href="/<?php echo $var;?>/xbrl-training-tutorials-xbrl-online-training">Training</a></li>
									<li><a href="/<?php echo $var;?>/xbrl-service-provider-and-software-vendor">About Us</a></li>
									<li><a href="/<?php echo $var;?>/contact-us">Contact Us</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="footer-widget">
								<h2>Services</h2>
								
										<ul class="sub-menu">
											<li><a href="/<?php echo $var;?>/annual-financial-report-balance-sheet-profit-loss-account-xbrl-filing">Annual Financial Report</a></li>
											<li><a href="/<?php echo $var;?>/cost-audit-compliance-audit-report-in-xbrl-cost-audit-report-in-xbrl-volition-llp">Cost Audit &amp; Compliance Audit Report</a></li>
											<li><a href="/<?php echo $var;?>/corporate-governance-in-xbrl-shareholding-pattern-in-xbrl-volition-llp">Corprate Governance Report &amp; Shareholding Pattern</a></li>
											<li><a href="xbrl-xml-to-pdf.html">XML to PDF</a></li>
										</ul>
						
							</div>
						</div>
						<div class="col-sm-4">
							<div class="footer-widget">
								<h2>Quick Links</h2>
								<ul>
									<li><a href="/xbrl-outsourcing-services-conversion-services">Outsourcing</a></li>
									<li><a href="/partners-looking-for-xbrl-partners">Partners</a></li>
									<li><a href="/xbrl-software-free-download">XBRL Software</a></li>

									<li><a href="/careers">Careers</a></li>
									<li><a href="/xbrl-download-software-validation-tool-taxonomy">Downloads</a></li>
									<li><a href="/xbrl-glossary">XBRL A to Z</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-5 order-lg-1">
					<img src="img/logo.png" alt="volition">
					<div class="copyright">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Developed <i class="fa fa-ravelry" aria-hidden="true"></i> by <a href="#" target="_blank">xbrlinindia.com</a></div>
					<div class="social-links">
						<a href=""><i class="fa fa-instagram"></i></a>
						<a href=""><i class="fa fa-pinterest"></i></a>
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-twitter"></i></a>
						<a href=""><i class="fa fa-youtube"></i></a>
					</div>
				</div>
			</div>

		</div>
	</footer>
	<footer class="postfooter-section">
		
					<a style="color:#6a7080;" href="/<?php echo $var;?>/privacy_policy" target="_blank">Privacy Policy</a>
				
				     &nbsp;

					<a style="color:#6a7080;" href="/<?php echo $var;?>/sitemap" target="_blank">Sitemap</a>

					 &nbsp;

					<a style="color:#6a7080;" href="/<?php echo $var;?>/legal_disclaimer" target="_blank">Legal Disclaimer</a>
			
	</footer>
	<!-- Footer section end -->
	
	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/mixitup.min.js"></script>
	<script src="js/main.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  
	</body>
</html>
