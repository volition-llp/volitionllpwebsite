<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h3>Training</h3>
					</div>
				</div>
				<div class="col-md-8">
					<h4>XBRL Training</h4>
					<p><a href="/xbrl-training-tutorials-xbrl-online-training">XBRL Training</a> is one of the effective ways to understand the concept of XBRL. Learn from our experts about XBRL, XBRL Software, XBRL Mapping, XBRL Validation.</p>

					<p>The training will not only help you understand various complexities of XBRL Conversion, but will also make you self reliant on XBRL Filing to the regulators.</p>

					<p>To undertake XBRL Training or Seminar on XBRL write to 
						<a href="mailto:sachin.trivedi@volitionllp.com">Volition LLP</a> </p>
					<br><br>
					


					<div class="row">
					<div class="col-md-1">
					</div>
						<div class="col-md-11">
							
								<ul>
								<li><a href="#bangalore"style="border-color: #fff !important;">XBRL Training in Bangalore</a></li>
								<li><a href="#mumbai" style=" border-color: #fff !important;">XBRL Training in Mumbai</a></li>
								<li><a href="#delhi"  style=" border-color: #fff !important;">XBRL Training in Delhi</a></li>

								<li><a href="#delhi"style=" border-color: #fff !important;">XBRL Training in Delhi</a></li>
								<li><a href="#kolkata"  style=" border-color: #fff !important;">XBRL Training in Kolkata</a></li>
							   </ul>

						</div>

					</div>
			


					<div class="row top-buffer">
					<div class="col-md-10">
					 <a href="#" class="site-btn">Click to know about XBRL Services</a>
					</div>
					</div>

					
				</div>
				<div class="col-lg-4">
						<!-- Subscription section -->
	<?php include('countrysection.php'); ?>

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
