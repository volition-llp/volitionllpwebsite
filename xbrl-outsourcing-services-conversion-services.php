<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>Outsourcing Services</h3>
					</div>
				</div>
				<div class="col-lg-8">
					
					<div class="row">
						<div class="col-md-4">
							<a href="/xbrl-outsourcing-services-conversion-services" title="United States" style="color:#953735; text-decoration: none; font-weight:bold">XBRL Services INDIA</a>

						</div>

						<div class="col-md-4">
							<a href="/xbrl-outsourcing-services-us-sec-requirement" title="United Kingdom" style="color:#953735; text-decoration: none; font-weight:bold">XBRL Services US</a>
						</div>

						<div class="col-md-4">
							<a href="/volition-llp-ixbrl-outsourcing-services-uk" title="Singapore" style="color:#953735; text-decoration: none; font-weight:bold">XBRL Services UK</a>
						</div>

					</div>
			
					<div class="row top-buffer">
					<div class="col-md-10">
					  <p class="span"><a href="/">Volition LLP</a> has assisted some of the largest corporate with XBRL Conversion Services. We have developed an inhouse team of specialists who take an extra care to make sure client's get quality deliverables on time.</p>
					</div>
					</div>
					<div class="row top-buffer">
						<div class="col-md-1">
						</div>
					<div class="col-md-11">
					   <ul>
						<li>Your company does not need to buy software, wait for upgrades or handle unexpected errors</li>
						<li> Your company does not need to continually hire & train new people on software or web applications</li>
						<li>We have a strong pool of XBRL & iXBRL specialists</li>
						<li>The specialists review the entire process of XBRL Conversion of your company</li>
						<li>Our expertise & understanding of accounts and your specific needs helps to deliver quality XBRL documents</li>
						<li>Our team of specialists can save your time & money</li>
					   </ul>
					</div>
					</div>


					<div class="row top-buffer">
					<div class="col-md-10">
					 <p class="text-justify">If you are a Company with a net turnover of upto Rs 100 crores or a Private Limited Company (Pvt. Ltd. Co.). ASK for Trial XBRL Conversion and get a Training for your Company. Contact us <a href="mailto:sachin.trivedi@volitionllp.com">sachin.trivedi@volitionllp.com</a> to ask for XBRL Conversion Services for your company
					 </p>
					</div>
					</div>


					<div class="row top-buffer">
					<div class="col-md-10">
					 <a href="#" class="site-btn">Click to know about XBRL Services</a>
					</div>
					</div>

					
				</div>
				<div class="col-lg-4">
						<!-- Subscription section -->
        <?php include('countrysection.php'); ?>
	<!-- Subscription section end -->

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
