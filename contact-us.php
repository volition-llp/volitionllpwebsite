<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Contact section -->
	<section class="contact-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 p-0">
					<!-- Map -->
					<div class="map"><iframe class="frame" src="https://www.google.com/maps/embed?pb=!1m22!1m8!1m3!1d3686.0070628556637!2d88.33086296495836!3d22.503918185218186!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d22.504018199999997!2d88.3336466!4m5!1s0x3a0270af8aac2c55%3A0x3ef0fbb2f3cc95f0!2sVolition%20LLP%2C%20S-%2041%2C%20Bangur%20Complex%205%2F7%20B.S.T%2C%20Main%20Road%2C%20Kolkata%2C%20West%20Bengal%20700038!3m2!1d22.50404!2d88.332416!5e0!3m2!1sen!2sin!4v1604131934230!5m2!1sen!2sin" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
				</div>
				<div class="col-lg-6 p-0">
					<div class="contact-warp">
						<div class="section-title mb-0">
							<h2>Get in touch</h2>
						</div>
						<p> </p>
						<ul>

							<li>Mr. Sachin Trivedi</li>
							<li>sachin.trivedi@volitionllp.com</li>
							<li>+91-99030-37609</li>
						</ul>
						<form class="contact-from">
							<div class="row">
								<div class="col-md-6">
									<input type="text" placeholder="Your name">
								</div>
								<div class="col-md-6">
									<input type="text" placeholder="Your e-mail">
								</div>
								<div class="col-md-12">
									<input type="text" placeholder="Mobile No.">
									<input type="text" placeholder="Country">
									<input type="text" placeholder="Company Name">
									<input type="text" placeholder="Subject">
									<textarea placeholder="Message"></textarea>
<html>
  <head>
    <title>reCAPTCHA demo: Simple page</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  </head>
  <body>
    <form action="?" method="POST">
      <div class="g-recaptcha" data-sitekey="your_site_key"></div>
      <br/>
      <button class="site-btn">send message</button>
    </form>
  </body>
</html>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog section end -->

<?php
include_once('footer/footer.php');
?>

