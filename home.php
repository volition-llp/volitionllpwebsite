<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Hero section -->
	<section class="hero-section">
		<div class="hero-slider owl-carousel">
			<div class="hs-item">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="hs-text">
								<h2><span>XBRL</span> Conversion Services.</h2>
								<p>Quick conversion of financial statements, cost audit reports to XBRL for MCA &amp SEBI Filings.</p>
								<a href="#" class="site-btn">Conversion Services</a>
								<a href="#" class="site-btn sb-c2">Contact Us</a>
							</div>
						</div>
						<div class="col-lg-6">
							
						</div>
					</div>
				</div>
			</div>
			<div class="hs-item">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="hs-text">
								<h2><span>Excel to XBRL</h2>
								<p>Convert excel to XBRL &amp iXBRL for filings across geographies. Quick conversion of XBRL &amp iXBRL reports to word and PDF formats. </p>
								<a href="#" class="site-btn">Conversion Services</a>
								<a href="#" class="site-btn sb-c2">Contact Us</a>
							</div>
						</div>
						<div class="col-lg-6">
							
						</div>
					</div>
				</div>
			</div>
				<div class="hs-item">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="hs-text">
								<h2><span>XBRL to PDF</h2>
								<p>Convert excel to XBRL &amp iXBRL for filings across geographies. Quick conversion of XBRL &amp iXBRL reports to word and PDF formats. </p>
								<a href="#" class="site-btn">Conversion Services</a>
								<a href="#" class="site-btn sb-c2">Contact Us</a>
							</div>
						</div>
						<div class="col-lg-6">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end -->

  
 
	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="section-title">
						<h2>Digital Reporting Services</h2>
					</div>
				</div>
				<div class="col-lg-6">
					<p>Volition LLP is a management consulting firm, advising organizations on emerging issues like IFRS and XBRL. Volition LLP has been assisting some of the largest companies of India with IFRS Conversion and is set to bridge the digital divide with XBRL Services and XBRL Software.</p>
					<a href="#" class="site-btn">Read More</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->



	<!-- How section -->
	<section class="how-section spad set-bg" data-setbg="img/how-to-bg.jpg">
		<div class="container text-white">
			<div class="section-title">
				<h3>Latest MCA Circulars, Tools &amp Taxonomy</h3>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="how-item">
						<div class="hi-icon">
							<img src="img/icons/pointer.png" alt="">
						</div>
						<h4>MCA Circulars</h4>
						<p>Check out all the MCA Circulars, from extnsion of XBRL filing date, latest AOC-4 forms, cost audit filing, additional disclosures and more.</p>
						<a href="#" class="site-btn sb-c2">Check Circulars</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="how-item">
						<div class="hi-icon">
							<img src="img/icons/pointer.png" alt="">
						</div>
						<h4>Validation Tools</h4>
						<p>Find latest MCA XBRL validation tools, for C&ampI and cost audit reporting. Download all previous versions of the XBRL validation tool here.</p>
						<a href="#" class="site-btn sb-c2">Latest Validation Tool</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="how-item">
						<div class="hi-icon">
							<img src="img/icons/pointer.png" alt="">
						</div>
						<h4>Taxonomy &amp Business Rules</h4>
						<p>Get all versions of Business Rules &amp Taxonomy. Useful for both developers &amp for reviewers of XBRL financial statements &amp cost audit reports.</p>
						<a href="#" class="site-btn sb-c2">Latest Taxonomy</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- How section end -->

		<!-- Intro section -->
	<section class="intro-section spad countrymbilesec" style="display:none;">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="section-title">
						<h2>Digital Reporting Services in other countries</h2>
					</div>
				</div>
				<div class="col-lg-6">
					<table class="table-responsive custom-table-design">
					  <tbody>
					    <tr>
					      <td><h3><a href="/" class="countrycolor">India</a></h3></td>
					      <td><h3><a href="/xbrl-outsourcing-services-us-sec-requirement.php" class="countrycolor">US</a></h3></td>
					    </tr>
					    <tr>
					      <td><h3><a href="#" class="countrycolor">Malaysia</a></h3></td>
					      <td><h3><a href="#" class="countrycolor">	South Africa</a></h3></td>
					    </tr>
					    <tr>
					      <td><h3><a href="/XBRL-filing-requirements-Singapore-ACRA-Bizfile-tax-filing" class="countrycolor">Singapore</a>
										</h3></td>
					      <td><h3>
											<a href="/XBRL-filing-of-financial-statements-to-Revenue-Ireland" class="countrycolor">Ireland</a>
										</h3></td>
					    </tr>
				
					    <tr>
					     
					      <td><h3><a href="/volition-llp-ixbrl-outsourcing-services-uk" class="countrycolor">UK</a>
										</h3></td>
					    </tr>
					  </tbody>
					</table>
				
				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	<section class="intro-section spad" >
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="section-title">
						<h2>Digital Reporting Services in other countries</h2>
					</div>
				</div>
				<div class="col-lg-6">
					?Demo conte nst
				</div>
			</div>
		</div>
	</section>


<?php
include_once('footer/footer.php');
?>
