
<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>
<nav class="navbar navbar-default custom-navbar" id="navbar">
	<div class="container-fluid">
  				<a href="/" style="color:#0a183d;">India</a>

						|<a href="/xbrl-outsourcing-services-us-sec-requirement.php" style="color:#0a183d;">US</a>

						|<a href="#" style="color:#0a183d;">Malaysia</a>

						|<a href="#" style="color:#0a183d;">South Africa</a>

						|<a href="/XBRL-filing-requirements-Singapore-ACRA-Bizfile-tax-filing" style="color:#0a183d;">Singapore</a>

						|<a href="/XBRL-filing-of-financial-statements-to-Revenue-Ireland" style="color:#0a183d;">Ireland</a>

						|<a href="/volition-llp-ixbrl-outsourcing-services-uk" style="color:#0a183d;">UK</a>
					</div>
</nav>
	<!-- Header section -->
	<header class="header-section clearfix">
		<a href="index.html" class="site-logo">
			<img src="img/logo.png" alt="">
		</a>
		
		</div>
		<ul class="main-menu">
			<li><a href="/<?php echo $var;?>/">Home</a></li>
			<li><a href="/<?php echo $var;?>/conversion-services-software">Services</a>
				<ul class="sub-menu">
					<li><a href="/<?php echo $var;?>/annual-financial-report-balance-sheet-profit-loss-account-xbrl-filing">Annual Financial Report</a></li>
					<li><a href="/<?php echo $var;?>/cost-audit-compliance-audit-report-in-xbrl-cost-audit-report-in-xbrl-volition-llp">Cost Audit &amp; Compliance Audit Report</a></li>
					<li><a href="/<?php echo $var;?>/corporate-governance-in-xbrl-shareholding-pattern-in-xbrl-volition-llp">Corprate Governance Report &amp; Shareholding Pattern</a></li>
					<li><a href="xbrl-xml-to-pdf.html">XML to PDF</a></li>
				</ul>
			</li>
			<li><a href="#">Circulars</a></li>
			<li><a href="/<?php echo $var;?>/xbrl-training-tutorials-xbrl-online-training">Training</a></li>
			<li><a href="/<?php echo $var;?>/xbrl-service-provider-and-software-vendor">About Us</a></li>
			<li><a href="/<?php echo $var;?>/contact-us">Contact Us</a></li>
		</ul>
	</header>
	<!-- Header section end -->