<!DOCTYPE html>
<html lang="en">
<head>
	<title>Volition LLP</title>
	<meta charset="UTF-8">
	<meta name="description" content="Volition LLP has been assisting some of the largest companies of India with XBRL Conversion Services and XBRL Software. Volition is set to bridge the digital divide by keeping you updated with developments on XBRL in India, MCA Circular on XBRL, Updated XBRL Taxonomy, Free XBRL Software Download, XBRL Validation Tool.">
	<meta name="keywords" content="XBRL in India, XBRL Conversion Services India, XBRL Conversion Services in India, XBRL Service Providers in India, XBRL Conversion Services for Power Sector, XBRL Conversion Services for Mutual Funds, XBRL Conversion Services for Insurance, XBRL Conversion Services for NBFCs, XBRL Filing Services for SEBI Filings, XBRL for pvt ltd co, Extension of Last Date of filing of AOC-4, AOC4 XBRL, General Circular, Corporate Governance – Regulation 27 (only in XBRL mode), Shareholding Pattern – Regulation 31 (only in XBRL mode)">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
 
	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="css/slicknav.min.css"/>
    <link rel="stylesheet" href="css/custom.css"/>
	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css"/>


	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<?php $var = "volition_website"; ?>
</head>
<body>
