<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>Corporate Governance in XBRL Shareholding Pattern in XBRL</h3>
					</div>
				</div>
				<div class="col-lg-8">
					<h4>XBRL Filing Applicability Criteria</h4>
					<p>As per the SEBI regulations and under its circular, the Regulation 10 of the Securities and Exchange Board of India (Listing Obligations and Disclosure Requirements) Regulations, 2015 (“Listing Regulations”) which reads as follows:</p>

					<p>10. (1) The listed entity shall file the reports, statements, documents, filings and any other information with the recognised stock exchange(s) on the electronic platform as specified by the Board or the recognised stock exchange(s).
					(2) The listed entity shall put in place infrastructure as required for compliance with sub-regulation (1).</p>

					<p >Listed Entities are hereby informed that with effect from 21st March, 2016, submissions required to be filed in compliance to the below mentioned Regulations of the Listing Regulations shall be accepted only through the Listing Centre:-</p>
					<br><br>

				    <ul class="sub-list-custom" style="margin-top: -4em;">
						<li><a href="#">Compliance Certificate by Share Transfer Agent – Regulation 7(3)</a></li>
						<li><a href="#">Statement of Investor Complaints – Regulation 13(3)</a></li>
						<li><a href="#">Corporate Governance – Regulation 27 (only in XBRL mode)</a></li>
						<li><a href="#">Notice for Board Meeting – Regulation 29</a></li>
						<li><a href="#">Outcome of Board Meeting – Regulation 30</a></li>
						<li><a href="#">Shareholding Pattern – Regulation 31 (only in XBRL mode)</a></li>
						<li><a href="#">Financial Results – Regulation 33</a></li>
						<li><a href="#">Annual Report – Regulation 34</a></li>
						<li><a href="#">Compliance Certificate – Regulation 40(9)</a></li>
						<li><a href="#">Notice for Record Date – Regulation 42</a></li>
						<li><a href="#">Voting Result – Regulation 44</a></li>
						<li><a href="#">Disclosures under SAST and PIT Regulations (Submissions by company)</a></li>
						<li><a href="#">Reconciliation of Share Capital Audit Report – Regulation 55A (Depositories and Participants Regulations, 1996)</a></li>
				    </ul>

					</p>
					<a href="#" class="site-btn">Click to know about XBRL Services</a>
				</div>
				<div class="col-lg-4">
						<!-- Subscription section -->
	<?php include('countrysection.php'); ?>
	<!-- Subscription section end -->

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
