<?php
include_once('header/header_home.php');
include_once('header/menu_header.php');
?>

	<!-- Intro section -->
	<section class="intro-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h3>Legal Disclaimer</h3>
					</div>
				</div>
				<div class="col-lg-10">
					
					<h4>Volition Business Solutions LLP ("Volition LLP") is a firm registered in India.</h4>

					
					<div class="row top-buffer">
					<div class="col-md-10">
						<h5><strong>XBRLinIndia.com</strong> provides no client services. Services are provided solely by Volition LLP.</h5>
					
					
					 <p class="text-justify">The information contained and accessed on this site (the "Site") is provided by the Volition LLP entity indicated on the homepage as owner of the Site ("XBRLinIndia.com") for general guidance and is intended to offer the user general information of interest. The information provided is not intended to replace or serve as substitute for any compliance, local law, audit, tax or other professional advice, consultation or service. You should consult with a Volition LLP professional in the respective professional area to obtain such services.


					 </p>
					
					 <p class="text-justify">All content on the Site and all services provided through it are provided "as is", with no guarantees of completeness, accuracy or timeliness, and without representations, warranties or other contractual terms of any kind, express or implied. Volition LLP does not represent or warrant that this Site, the various services provided through this Site, and / or any information, software or other material downloaded from this Site, will be accurate, current, uninterrupted, error-free, omission-free or free of viruses or other harmful components.</p>
				
					  <p class="text-justify">In no event shall Volition LLP or any of their respective partners, principals, agents or employees, be liable for any direct, indirect, incidental, special, exemplary, punitive, consequential or other damages (including but not limited to, liability for loss of use, data or profits), without regard to the form of any action, including but not limited to, contract, negligence or other tortuous actions, arising out of or in connection with the Site, any content on or accessed by use of the Site, or any copying, display or other use.</p>
					  
					  <p class="text-justify">
					Materials on the Site may not be modified, reproduced, publicly displayed, performed, distributed or used for any public or commercial purposes without explicit written permission from the appropriate content or material provider (including third-party links). Volition LLP bears no risk, responsibility or liability in the event that a user does not obtain such explicit written permission as advised by Volition LLP.
						</p>

						 <p class="text-justify">
					Third-party links are provided as a convenience to our users. Volition LLP does not control and is not responsible for any of these sites or their content.
						</p>

					</div>
					</div>

					
				</div>
				<div class="col-lg-2">
						

				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	
<?php
include_once('footer/footer.php');
?>
